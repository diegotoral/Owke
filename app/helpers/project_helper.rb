module ProjectHelper
  def project_progress(project)
    content_tag :div, class: 'progress progress-xxs' do
      content_tag :div, '', class: 'progress-bar success', style: "width: #{project.progress}%"
    end
  end
end
