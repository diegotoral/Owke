class SignUpsController < ApplicationController
  before_action :set_sign_up_form, only: %i(new create)

  def new; end

  def create
    sign_up = Company::SignUp.new

    sign_up.on(:success) { |company| redirect_to_dashboard(company) }
    sign_up.on(:error) do
      flash[:alert] = 'Failed to sign up company'
      render :new
    end

    sign_up.call(@sign_up_form, params[:sign_up])
  end

  def confirm
    confirm_sign_up = Company::ConfirmSignUp.new

    confirm_sign_up.subscribe(Tenant::Switch, on: :company_loaded, with: :call)

    confirm_sign_up.on(:success) do |company|
      redirect_to_dashboard(company, notice: 'Email confirmed')
    end

    confirm_sign_up.on(:error) do
      flash[:alert] = 'Failed to confirm your account'
      redirect_to root_path
    end

    confirm_sign_up.call(params[:company], params[:token])
  end

  private

  def company
    Company.new(owner: User.new)
  end

  def set_sign_up_form
    @sign_up_form ||= SignUpForm.new(company)
  end

  def redirect_to_dashboard(company, options = {})
    redirect_to dashboard_url(subdomain: company.subdomain), options
  end
end
