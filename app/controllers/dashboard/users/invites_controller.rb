module Dashboard
  class Users::InvitesController < Dashboard::BaseController
    before_action :set_invite_form, only: %i(new create)

    def index
      @invites = User::Invite.pending
    end

    def new; end

    def create
      create_invite = Invite::Create.new

      create_invite.on(:success) do |invite|
        respond_with invite, location: dashboard_invites_path
      end

      create_invite.on(:error) do |form|
        respond_with form.sync
      end

      create_invite.call(@invite_form, current_company, params[:user_invite])
    end

    def destroy
      @invite = User::Invite.find(params[:id])
      @invite.destroy

      redirect_to dashboard_invites_path
    end

    private

    def set_invite_form
      @invite_form ||= User::InviteForm.new(User::Invite.new)
    end
  end
end
