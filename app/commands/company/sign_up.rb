class Company::SignUp < ApplicationCommand
  def call(form, params)
    if form.validate(params)
      company = form.sync

      ActiveRecord::Base.transaction do
        Company::Create.call(company)
        Tenant::Switch.call(company)
        Company::CreateOwner.call(company, company.owner)
      end

      Company::SignUpMailer.welcome(company).deliver_later

      broadcast(:success, company)
    else
      broadcast(:error, form.errors)
    end
  rescue ActiveRecord::RecordInvalid
    broadcast(:error, form.errors)
  end
end
