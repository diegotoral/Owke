class Dashboard::UsersController < Dashboard::BaseController
  skip_before_action :require_resource!, only: %i(new create)
  before_action :set_user, only: %i(show destroy)

  def index
    @users = User.all
  end

  def show; end

  def new
    user = User.new
    redeem_invite = RedeemInvite.new
    @user_form = UserForm.new(user)

    redeem_invite.on(:success) do |user|
      user = user
      render :new, layout: false
    end

    redeem_invite.on(:error) do
      render :new, layout: false
    end

    redeem_invite.call(params[:token])
  end

  def create
    create_user = User::Create.new

    create_user.on(:success) do
      redirect_to dashboard_url
    end

    create_user.on(:error) do
      flash[:alert] = 'Failed to create your account'
      redirect_to dashboard_url
    end

    create_user.call(params[:user])
  end

  def destroy
    @user.delete

    redirect_to dashboard_users_path
  end

  private

  def set_user
    @user ||= User.find(params[:id])
  end
end
