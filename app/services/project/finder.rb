class Project::Finder
  class << self
    def all
      resource_class.includes(:customer).all
    end

    private

    def resource_class
      ::Project
    end
  end
end
