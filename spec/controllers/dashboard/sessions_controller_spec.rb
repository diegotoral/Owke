require 'rails_helper'

RSpec.describe Dashboard::SessionsController, type: :controller do
  let(:current_user) { double(:user, name: 'Fulano de Tal') }

  before do
    allow(controller).to receive(:current_user).and_return(current_user)
  end

  describe 'GET on #new' do
    it 'responds with 200 OK' do
      get :new

      expect(response).to have_http_status 200
    end
  end

  describe 'POST on #create' do
    let(:user_session) {
      {
        email: 'some@email.com',
        password: '123456',
        password_confirmation: '123456',
      }
    }

    context 'on success' do
      it "saves user's session" do
        expect_any_instance_of(User::Session).to receive(:save)

        post :create, params: { user_session: user_session }
      end

      it 'redirects to dashboard' do
        allow_any_instance_of(User::Session).to receive(:save).and_return(true)

        post :create, params: { user_session: user_session }

        expect(response).to redirect_to dashboard_path
      end
    end
  end

  describe 'DELETE on #destroy' do
    it 'redirects to new session' do
      session = double(:session, record: true, destroy: true)
      allow(controller).to receive(:current_user_session).and_return(session)

      delete :destroy

      expect(response).to redirect_to new_dashboard_sessions_path
    end
  end
end
