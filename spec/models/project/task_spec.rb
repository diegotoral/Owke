require 'rails_helper'

RSpec.describe Project::Task, type: :model do
  it { is_expected.to define_enum_for(:status) }

  it { is_expected.to belong_to :project }
  it { is_expected.to belong_to :responsible }
end
