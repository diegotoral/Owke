require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to have_many :projects }

  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_presence_of :email }
  it { is_expected.to validate_presence_of :initials }

  describe 'factories' do
    it 'is valid' do
      expect(build(:user)).to be_valid
    end
  end

  describe '#confirmed!' do
    it 'sets confirmed to true' do
      user = build(:user, confirmed: false)

      user.confirmed!

      expect(user).to be_confirmed
    end
  end
end
