class Project < ApplicationRecord
  belongs_to :customer
  belongs_to :responsible, polymorphic: true

  has_many :tasks, dependent: :delete_all

  def progress
    total = tasks.count
    return 0 if total == 0
    completed = tasks.finished.count
    (completed * 100) / total
  end

  def global_responsible
    self.responsible.to_global_id if responsible.present?
  end

  def global_responsible=(responsible)
    self.responsible = GlobalID::Locator.locate responsible
  end
end
