class Dashboard::CustomersController < Dashboard::BaseController
  before_action :set_customer, only: %i(destroy)
  before_action :set_customer_form, except: %i(index)

  def index
    @customers = Customer.all
  end

  def new; end

  def create
    if @customer_form.validate(params[:customer])
      @customer_form.save
    end

    respond_with @customer_form.sync, location: dashboard_customers_path
  end

  def edit; end

  def update
    if @customer_form.validate(params[:customer])
      @customer_form.save
    end

    respond_with @customer_form.sync, location: dashboard_customers_path
  end

  def destroy
    @customer.delete
    respond_with @customer, location: dashboard_customers_path
  end

  private

  def customer
    @customer ||= Customer.find_or_initialize_by(id: params[:id])
  end

  def set_customer_form
    @customer_form ||= CustomerForm.new(customer)
  end
end
