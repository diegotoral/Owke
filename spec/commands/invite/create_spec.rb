require 'rails_helper'

RSpec.describe Invite::Create, type: :command do
  subject(:command) { described_class.new(mailer: mailer) }

  let(:invite) { build_stubbed(:user_invite) }
  let(:company) { build_stubbed(:company) }
  let(:mailer) { double(UserMailer) }
  let(:form) { double(:form, sync: invite) }

  context 'on success' do
    before do
      allow(invite).to receive(:invite!)
      allow(form).to receive(:validate).and_return(true)
    end

    it 'sends invite by email' do
      params = { name: 'Fulano', email: 'fulano@example.com' }

      expect(mailer).to receive(:invite)
        .with(company.id, invite.id)
        .and_return(mailer)

      expect(mailer).to receive(:deliver)

      command.call(form, company, params)
    end

    it 'broadcasts :success with the invite' do
      params = { name: 'Fulano', email: 'fulano@example.com' }

      expect(mailer).to receive(:invite)
        .with(company.id, invite.id)
        .and_return(mailer)

      allow(mailer).to receive(:deliver)

      expect { command.call(form, company, params) }.to broadcast(:success, invite)
    end
  end

  context 'on error' do
    it 'broadcasts :error with details' do
      allow(form).to receive(:validate).with({}).and_return(false)

      expect { command.call(form, company, {}) }.to broadcast(:error, form)
    end
  end
end
