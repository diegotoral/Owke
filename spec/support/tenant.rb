RSpec.configure do |config|
  config.include TenantHelpers

  config.before(:suite) do
    Apartment::Tenant.drop('app') rescue nil

    company = Company.new(name: 'Test Corp.', subdomain: 'app')
    Company::Create.call(company)
  end

  config.before(:each) do
    Apartment::Tenant.switch! 'app'
  end

  config.after(:each) do
    Apartment::Tenant.reset
  end
end
