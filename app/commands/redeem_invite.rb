class RedeemInvite < ApplicationCommand
  def call(invite_code)
    invite = User::Invite.find_by!(redeemed_at: nil, invite_code: invite_code)
    user = User.new(invite.attributes.slice(:name, :email))
    invite.redeemed!

    broadcast(:success, user)
  rescue ActiveRecord::RecordNotFound
    broadcast(:error)
  end
end
