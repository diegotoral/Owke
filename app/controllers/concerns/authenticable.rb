# = Autheticable concern
#
# Authenticable defines commom methods for authenticating a `resource`.
# It expects a `Session` class that responds to `find` and returns the
# autheticated resource. This class must be namespaced on the `resource`.
#
# For example, for a `User` resource Authenticable expects a session class
# named `User::Session`.
#
module Authenticable
  extend ActiveSupport::Concern

  included do
    mattr_accessor :authenticable_resource
    before_action :require_resource!, if: :authenticable_resource_defined?
  end

  class_methods do
    def authenticate(resource)
      self.authenticable_resource = resource

      self.class_eval do
        define_method("current_#{resource}_session") do
          session_variable_name = "@current_#{resource}_session"
          session_class = "#{resource.to_s.classify}::Session".constantize
          instance_variable_set(session_variable_name, session_class.send(:find))
        end

        define_method("current_#{resource}") do
          session_instance = send("current_#{resource}_session")
          instance_variable_set("@current_#{resource}", session_instance.record) if session_instance.present?
        end
      end
    end
  end

  def require_resource!
    current_resource = send("current_#{self.authenticable_resource}")

    if current_resource.blank?
      store_location
      send(:redirect_to_login)
      return false
    end
  end

  def store_location
    session[:return_to] = request.path
  end

  def authenticable_resource_defined?
    self.authenticable_resource.present?
  end
end
