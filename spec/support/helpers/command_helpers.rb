module CommandHelpers
  class StubbedCommand < ApplicationCommand; end

  def stub_command(klass, event_name, *event_args)
    stubbed_command = Class.new(StubbedCommand) do
      define_method(:call) do |*args|
        broadcast(event_name, event_args)
      end
    end

    stub_const(klass, stubbed_command)
  end
end

RSpec.configure do |config|
  config.include CommandHelpers, type: :controller
end
