class Dashboard::BaseController < ::ApplicationController
  include Authenticable

  layout 'dashboard'

  authenticate :user

  helper_method :current_company

  rescue_from ActiveRecord::RecordNotFound do
    render status: :not_found
  end

  protected

  def current_tenant
    @current_tenant ||= Apartment::Tenant.current
  end

  def current_company
    @current_company ||= Company.find_by(subdomain: current_tenant)
  end

  def redirect_to_login
    redirect_to new_dashboard_sessions_url
  end
end
