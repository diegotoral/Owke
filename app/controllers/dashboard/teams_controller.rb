class Dashboard::TeamsController < Dashboard::BaseController
  def index
    @teams = Team.all.includes(:members)
  end

  def new
    @users = User.all
    @team = Team.new
  end

  def create
    @team = Team.new(team_params)
    @team.team_memberships.build(members.map {|m| { user_id: m } })

    unless @team.save
      @users = User.all
    end

    respond_with @team, location: [:dashboard, :teams]
  end

  def edit
    @users = User.all
    @team = Team.find(params[:id])
  end

  def update
    @users = User.all
    @team = Team.find(params[:id])
    @team.members = User.find(members)

    @team.update_attributes(team_params)

    respond_with @team, location: [:dashboard, :teams]
  end

  private

  def team_params
    params.require(:team).permit(:name)
  end

  def members
    (params[:team].require(:members) || []).reject(&:empty?)
  end
end
