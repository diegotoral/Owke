require 'rails_helper'

RSpec.describe SignUpsController, type: :controller do
  describe 'GET on #new' do
    it 'responds with 200 OK' do
      get :new

      expect(response).to have_http_status 200
    end
  end

  describe 'POST on #create' do
    context 'on success' do
      it "redirects to company's dashboard" do
        company = build(:company)
        stub_wisper_publisher('Company::SignUp', :call, :success, company)

        post :create, params: {}

        expect(response).to redirect_to dashboard_url(
          subdomain: company.subdomain
        )
      end
    end

    context 'on error' do
      it 'flashs an alert' do
        stub_wisper_publisher('Company::SignUp', :call, :error, [])

        post :create, params: {}

        expect(flash.now[:alert]).not_to be_nil
      end
    end
  end

  describe 'GET ON #confirm' do
    context 'on company_loaded' do
      it 'switches tenant for the specified company' do
        company = build(:company)
        stub_wisper_publisher(
          'Company::ConfirmSignUp',
          :call,
          :company_loaded,
          company
        )

        expect(Tenant::Switch).to receive(:call).with(company)

        # xhr here make the test pass since it doen't returns a response and
        # rails complains about a missing body response.
        get :confirm, params: {}, xhr: true
      end
    end

    context 'on success' do
      it "redirects to company's dashboard" do
        company = build(:company)
        stub_wisper_publisher(
          'Company::ConfirmSignUp',
          :call,
          :success,
          company
        )

        get :confirm, params: { token: 'some-token' }

        expect(response).to redirect_to dashboard_url(
          subdomain: company.subdomain
        )
      end
    end

    context 'on error' do
      it 'redirects to home page' do
        stub_wisper_publisher('Company::ConfirmSignUp', :call, :error)

        get :confirm, params: { token: 'invalid-token' }

        expect(response).to redirect_to root_url
      end
    end
  end
end
