class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name, null: false
      t.date :deadline
      t.text :description
      t.belongs_to :customer, foreign_key: true, index: true
      t.belongs_to :responsible, references: :users, index: true

      t.timestamps
    end

    add_foreign_key :projects, :users, column: :responsible_id, on_delete: :nullify
  end
end
