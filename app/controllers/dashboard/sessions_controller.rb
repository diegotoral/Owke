class Dashboard::SessionsController < Dashboard::BaseController
  skip_before_action :require_resource!, only: %i(new create)

  layout 'session'

  def new
    @session = User::Session.new

    respond_with @session
  end

  def create
    @session = User::Session.new(session_params)
    @session.save

    respond_with @session, location: dashboard_path
  end

  def destroy
    current_user_session.destroy
    respond_with current_user_session, location: new_dashboard_sessions_path
  end

  private

  def flash_interpolation_options
    { user_name: current_user.name } if current_user.present?
  end

  def session_params
    params
      .require(:user_session)
      .permit(:email, :password)
  end
end
