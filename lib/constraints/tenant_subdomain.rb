module Constraints
  class TenantSubdomain
    attr_reader :subdomains

    def matches?(request)
      request.subdomain.present? && subdomains.include?(request.subdomain)
    end

    protected

    def subdomains
      @subdomains ||= Apartment.tenant_names
    end
  end
end
