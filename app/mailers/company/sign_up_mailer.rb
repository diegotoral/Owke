class Company::SignUpMailer < ApplicationMailer
  def welcome(company)
    @company = company
    @owner = company.owner

    mail(to: @owner.email, subject: 'Welcome to Owke')
  end
end
