class Company::Create
  def self.call(company, tenant: Apartment::Tenant)
    company.tap do |c|
      c.save!
      tenant.create(c.subdomain)
    end
  end
end
