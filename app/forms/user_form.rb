class UserForm < Reform::Form
  property :name
  property :email
  property :initials
  property :password
  property :password_confirmation

  validates :initials, length: { is: 2 }
  validates :name, :email, :initials, presence: true
  validates :password, presence: true, confirmation: true
end
