FactoryGirl.define do
  factory :user_invite, class: 'User::Invite' do
    name 'Fulano de Tal'
    sequence(:email) { |n| "invite#{n}@example.com"}

    trait :pending do
      redeemed_at nil
    end

    trait :unsent do
      invite_code nil
      redeemed_at nil
    end

    trait :sent do
      invite_code 'ABC123'
      redeemed_at Time.current
    end
  end
end
