require 'rails_helper'

RSpec.describe Company, type: :model do
  subject(:company) { build(:company) }

  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_presence_of :subdomain }
  it { is_expected.to validate_uniqueness_of :subdomain }

  describe "factories" do
    it "is valid" do
      expect(subject).to be_valid
    end
  end

  describe "#set_owner" do
    it "sets the owner_id" do
      user = build_stubbed(:user)

      company.set_owner(user)

      expect(company.owner_id).to eq(user.id)
    end
  end
end
