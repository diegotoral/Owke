class User::Create < ::ApplicationCommand
  attr_reader :mailer, :user, :user_form

  def initialize(user_form: UserForm, mailer: UserMailer)
    @mailer = mailer
    @user_form = user_form
  end

  def call(params)
    form = user_form.new(user)

    if form.validate(params)
      form.save

      broadcast(:success, user)
    else
      broadcast(:error, form.errors)
    end
  end

  private

  def user
    @user ||= User.new(active: true, approved: true, confirmed: true)
  end
end
