class Company::CreateOwner
  def self.call(company, user)
    user.tap do |u|
      u.active = true
      u.approved = true
      u.save!

      company.set_owner(u)
      company.save!
    end
  end
end
