require 'rails_helper'

RSpec.describe Dashboard::Users::InvitesController, type: :controller do
  describe 'GET on #index' do
    it 'responds with 200 OK' do
      login(create(:user, :active))

      get :index

      expect(response).to have_http_status 200
    end
  end

  describe 'GET on #new' do
    it 'responds with 200 OK' do
      login(create(:user, :active))

      get :new

      expect(response).to have_http_status 200
    end
  end

  describe 'POST on #create' do
    context 'when success' do
      let(:invite_params) do
        { user_invite: { name: 'Fulano', email: 'fulano@example.com' } }
      end

      it 'redirects to invites index' do
        login(create(:user, :active))
        stub_command('Invite::Create', :success)

        post :create, params: invite_params

        expect(response).to redirect_to dashboard_invites_path
      end
    end
  end

  describe 'DELETE on #destroy' do
    context 'when success' do
      it 'redirects to invites index' do
        invite = create(:user_invite)
        login(create(:user, :active))

        delete :destroy, params: { id: invite.id }

        expect(response).to redirect_to dashboard_invites_path
      end
    end
  end
end
