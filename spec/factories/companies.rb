require 'ffaker'

FactoryGirl.define do
  sequence(:subdomain) {|n| "subdomain#{n}" }

  factory :company do
    name { FFaker::Company.name }
    subdomain { generate(:subdomain) }
  end
end
