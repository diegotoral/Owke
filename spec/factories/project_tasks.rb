FactoryGirl.define do
  factory :project_task, class: 'Project::Task' do
    title 'Tarefa 1'

    project
    association :responsible, factory: :user
  end
end
