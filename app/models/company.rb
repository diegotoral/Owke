class Company < ApplicationRecord
  belongs_to :owner, class_name: 'User', autosave: false

  validates :name, presence: true
  validates :subdomain, presence: true, uniqueness: true

  def set_owner(user)
    self[:owner_id] = user.id
  end
end
