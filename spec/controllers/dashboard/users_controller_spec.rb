require 'rails_helper'

RSpec.describe Dashboard::UsersController, type: :controller do
  describe 'GET on #index' do
    it 'responds with 200 OK' do
      login(create(:user, :active))

      get :index

      expect(response).to have_http_status 200
    end
  end

  describe 'GET on #show' do
    it 'responds with 200 OK' do
      user = create(:user, :active)
      login(user)

      get :show, params: { id: user }

      expect(response).to have_http_status 200
    end
  end

  describe 'GET on #new' do
    context 'on success' do
      before { stub_command('RedeemInvite', :success, user) }

      let(:user) { build(:user) }

      it 'responds with 200 OK' do
        get :new, params: { invite_code: 'valid_invite_code' }

        expect(response).to have_http_status 200
      end
    end

    context 'on error' do
      before { stub_command('RedeemInvite', :error) }

      it 'responds with 200 OK' do
        get :new, params: { invite_code: 'invalid_invite_code' }

        expect(response).to have_http_status 200
      end
    end
  end

  describe 'POST on #create' do
    context 'on success' do
      before { stub_command('User::Create', :success) }

      it 'redirects to the dashboard' do
        post :create, params: { invite_code: 'sometoken' }

        expect(response).to redirect_to dashboard_url
      end
    end

    context 'on error' do
      before { stub_command('User::Create', :error) }

      it 'responds with 200 OK' do
        post :create, params: { user: {} }

        expect(response).to redirect_to dashboard_url
      end

      it 'flashes an alert message' do
        login(create(:user, :active))

        post :create, params: { user: {} }

        expect(flash[:alert]).not_to be_nil
      end
    end
  end

  describe 'DELETE on #destroy' do
    it 'redirets to users index' do
      user = create(:user, :active)
      login(user)

      delete :destroy, params: { id: user }

      expect(response).to redirect_to dashboard_users_path
    end
  end
end
