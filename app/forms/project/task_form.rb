class Project::TaskForm < Reform::Form
  property :title
  property :deadline
  property :description
  property :project_id
  property :responsible_id

  validates :title, presence: true
end
