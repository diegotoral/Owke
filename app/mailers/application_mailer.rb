class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@owke.me'
  layout 'mailer'
end
