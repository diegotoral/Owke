class ApplicationCommand
  include Wisper::Publisher

  def call(*args)
    raise NotImplementedError
  end
end
