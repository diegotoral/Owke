module Dashboard::Projects
  class TasksController < Dashboard::BaseController
    before_action :set_project, only: %i(new create)
    before_action :set_task_form

    def show
      @task = Project::Task.find(params[:id])
    end

    def new; end

    def create
      params[:project_task][:project_id] = @project.id

      if @task_form.validate(params[:project_task])
        @task_form.save
      end

      respond_with @task_form.sync, location: [:dashboard, @project]
    end

    def edit; end

    def update
      if @task_form.validate(params[:project_task])
        @task_form.save
      end

      respond_with @task_form.sync, location: [:dashboard, @task.project]
    end

    def status
      new_status = params[:status].to_i
      @task = Project::Task.find(params[:task_id])

      if @task && Project::Task.statuses.values.include?(new_status)
        @task.update_attribute(:status, new_status)
      end
    end

    private

    def task
      @task ||= Project::Task.find_or_initialize_by(id: params[:id])
    end

    def set_project
      @project ||= Project.find(params[:project_id])
    end

    def set_task_form
      @task_form ||= Project::TaskForm.new(task)
    end
  end
end
