class CreateTeamMemberships < ActiveRecord::Migration[5.0]
  def change
    create_table :team_memberships do |t|
      t.belongs_to :user, foreign_key: true, index: true
      t.belongs_to :team, foreign_key: true, index: true

      t.timestamps
    end

    add_index :team_memberships, [:user_id, :team_id]
  end
end
