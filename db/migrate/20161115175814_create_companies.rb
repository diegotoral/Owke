class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :subdomain, unique: true

      t.timestamps

      t.index [:name, :subdomain]
    end
  end
end
