require 'rails_helper'

RSpec.describe RedeemInvite, type: :command do
  subject(:command) { described_class.new }

  let(:invite_code) { 'some_invite_code' }
  let(:invite) { build_stubbed(:user_invite) }

  context 'on success' do
    before do
      allow(User::Invite).to receive(:find_by!)
        .with(redeemed_at: nil, invite_code: invite_code)
        .and_return(invite)
    end

    it 'redeems the invite' do
      command.call(invite_code)

      expect(invite).to be_redeemed
    end

    it 'broadcasts :success' do
      expect { command.call(invite_code) }.to broadcast(:success)
    end
  end
end
