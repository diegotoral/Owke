namespace :owke do
  namespace :company do
    desc "Creates a new company"
    task :create, [:name, :subdomain] => :environment do |t, args|
      print "Creating company #{args.name}..."
      Company::Create.call(name: args.name, subdomain: args.subdomain)
      print " Done!\n"
    end
  end
end
