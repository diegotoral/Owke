class AddResponsibleTypeToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :responsible_type, :string
  end
end
