require 'rails_helper'
require_dependency 'user/invite'

RSpec.describe User::Invite, type: :model do
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_presence_of :email }

  describe 'scopes' do
    describe '.pending' do
      it "returns invitations where redeemed date isn't set" do
        expected_invites = create_list(:user_invite, 3, :unsent)
        create_list(:user_invite, 3, :sent)

        expect(described_class.pending).to include(*expected_invites)
      end
    end
  end

  describe '.find_redeemable' do
    it 'finds the first record for the invitation code' do
      create(:user_invite, :unsent, invite_code: 'foobar')

      invite = described_class.find_redeemable('foobar')

      expect(invite).not_to be_nil
    end
  end

  describe '#pending?' do
    it 'is true when redeemed_at is nil' do
      invite = build(:user_invite, :pending)

      expect(invite).to be_pending
    end
  end

  describe '#invited?' do
    it 'is true when invite_code and invited_at are present' do
      invite = build(:user_invite, invite_code: 'foobar', invited_at: Time.now)

      expect(invite).to be_invited
    end
  end

  describe '#invite!' do
    it 'generates an invitation code' do
      invite = build(:user_invite)

      invite.invite!

      expect(invite.invite_code).not_to be_nil
    end

    it 'sets the invitation date' do
      invite = build(:user_invite)

      invite.invite!

      expect(invite.invited_at).not_to be_nil
    end

    it 'saves the changes' do
      invite = build(:user_invite)

      invite.invite!

      expect(invite).to be_persisted
    end
  end

  describe '#redeemed!' do
    it 'sets redeemed date' do
      invite = build(:user_invite)

      invite.redeemed!

      expect(invite.redeemed_at).not_to be_nil
    end

    it 'saves the changes' do
      invite = build(:user_invite)

      invite.redeemed!

      expect(invite).to be_persisted
    end
  end

  describe '#redeemed?' do
    context 'when redeemed_at is empty' do
      it 'returns false' do
        invite = build(:user_invite, redeemed_at: nil)

        expect(invite.redeemed?).to eq(false)
      end
    end

    context 'when redeemed_at is not empty' do
      it 'returns true' do
        invite = build(:user_invite, redeemed_at: Time.current)

        expect(invite.redeemed?).to eq(true)
      end
    end
  end
end
