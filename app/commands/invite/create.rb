class Invite::Create < ApplicationCommand
  attr_reader :mailer

  def initialize(mailer: UserMailer)
    @mailer = mailer
  end

  def call(form, company, params)
    if form.validate(params)
      invite = form.sync

      invite.invite!
      mailer.invite(company.id, invite.id).deliver
      broadcast(:success, invite)
    else
      broadcast(:error, form)
    end
  end
end
