Rails.application.routes.draw do
  constraints Constraints::TenantSubdomain.new do
    get '/', to: 'dashboard/home#index'
  end

  namespace :admin do
    get '/', to: 'panel#index', as: 'panel'
  end

  namespace :dashboard do
    get '/', to: 'home#index'

    resources :teams
    resources :customers
    resource :sessions, only: %i(new create destroy)
    resources :users, only: %i(index show new create destroy)
    resources :invites, module: :users, except: %i(show update edit)

    resources :projects, shallow: true do
      resources :tasks, module: :projects do
        patch :status, format: :ujs
      end
    end
  end

  resource :sign_up, only: %i(new create) do
    get :confirm
  end

  root 'home#index'
end
