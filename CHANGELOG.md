# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).



## [Unreleased]

### Fixed
- Especifica host na configuração do `action_mailer` para ambiente de desenvolvimento.
- Corrige typo `@import 'ui/flashMessage/` em `dashboard/application.scss`.
- Resolve bug com autenticação retornando sessão de usuário inválida (nil).

### Added
- Adiciona link para logout na home do dashboard.
- Adiciona mais alguns testes para classes importantes do sistema.
- Email de confirmação de email do administrador da empresa.
- Cria ambiente de `staging` no Heroku, com configurações básicas.
- Redireciona usuário para o dashboard da empresa mesmo sem especificar o caminho completo `/dashboard` na URL.
- Documentação (README.md) dos procedimentos necessários para a completa configuração do projeto em ambiente de desenvolvimento (local).
- Implementação do layout das telas de login.
- Configuração de ferramentas de frontend (Sass, Neat, Bourbon, etc).
- Fluxo de sign up de empresa com registro de usuário `owner` e notificação por email.
- Autenticação multitenancy e multiresource baseado na gem Authlogic que permite autenticação e gerenciamento de sessões independentes entre resources e tenants.
- Estrutura de rotas para os diferentes componentes da aplicação (landing page, dashboard e admin) em namespaces próprios, garantindo completo isolamento de sub rotas.
- Configuração multitenancy com a gem Apartment usando `Company` como tenant.
- Configuração de servidor de iteração contínua no [CircleCI](http://circleci.com).
- Configuração de suíte de testes com RSpec, Capybara e SitePrism.
- Inicialização de projeto Rails 5.
