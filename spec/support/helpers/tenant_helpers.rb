module TenantHelpers
  def current_company
    Company.find_by_subdomain(Apartment::Tenant.current)
  end
end
