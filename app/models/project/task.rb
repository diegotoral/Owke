class Project::Task < ApplicationRecord
  enum status: { not_started: 0, started: 1, finished: 2 }

  belongs_to :project
  belongs_to :responsible, class_name: 'User'

  def to_partial_path
    'projects/tasks/task'
  end
end
