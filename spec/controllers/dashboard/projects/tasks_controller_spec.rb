require 'rails_helper'

RSpec.describe Dashboard::Projects::TasksController, type: :controller do
  let(:project) { create(:project) }
  let(:user) { create(:user, :active) }

  before { login(user) }

  describe 'GET on #show' do
    xcontext 'when task exists' do
      it 'responds with 200 OK' do
        task = create(:project_task)

        get :show, params: { id: task }

        expect(response).to have_http_status 200
      end
    end

    xcontext 'when task does not exist' do
      it 'responds with 404 NOT FOUND' do
        get :show, params: { id: 0 }

        expect(response).to have_http_status 404
      end
    end
  end

  describe 'GET on #new' do
    it 'responds with 200 OK' do
      get :new, params: { project_id: project }

      expect(response).to have_http_status 200
    end
  end
end
