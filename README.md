# Owke [![CircleCI](https://circleci.com/gh/diegotoral/Owke/tree/master.svg?style=svg)](https://circleci.com/gh/diegotoral/Owke/tree/master)

`Owke` is a generic project management system developed to support multiple companies (multitenancy/SaaS). `Owke` is build on love, some pain and a lot of Ruby and Vue code.

## Getting Started

### Installation

**1) Install all dependencies listed bellow**

- Ruby 2.3.1 (pro tip: use `rvm`)
- Node.js >= 6.4 (pro tip: use `nvm`)
- PostgreSQL >= 9.5

**2) clone the repository**

    $ git clone git@github.com:diegotoral/Owke.git && cd Owke

**3) Run `bin/setup`**

    $ bin/setup

**4) populate your database with development data**

    $ bin/rails db:seed

**5) setup your DNS**

`Owke` maps subdomains to tenants. To work we need a way to answer to requests on subdomains like `tenant.local.dev`. Here we present two ways to acheive that.

#### hosts file

Add entires to your `/etc/hosts` file as in the example bellow.

```
##
# Host Database
#
# localhost is used to configure the loopback interface
# when the system is booting.  Do not change this entry.
##
127.0.0.1	localhost
255.255.255.255	broadcasthost

127.0.0.1 tenant1.local.dev
127.0.0.1 tenant2.local.dev
```

#### dnsmasq

Depending on your needs a local DNS server like `dsnmasq` can make this step a lot easier.

After installing `dnsmasq` add an entry that answer `127.0.0.1` for every request to the top-level-domain `.dev`.

    $ echo 'address=/.dev/127.0.0.1' > $(brew --prefix)/etc/dnsmasq.conf
    $ sudo bash -c 'echo "nameserver 127.0.0.1" > /etc/resolver/dev'

Run `scutil --dns` to show all of your current resolvers and check that all requests for `.dev` will go to the DNS server at `127.0.0.1`.

**6) point your browser to http://local.dev:3000 and good work**

:smile:
