require 'authlogic/test_case'

module LoginHelpers
  include Authlogic::TestCase

  def login(user)
    activate_authlogic
    User::Session.create(user)
  end
end

RSpec.configure do |config|
  config.include LoginHelpers, type: :controller
end
