class User::InviteForm < Reform::Form
  property :name
  property :email

  validates :name, :email, presence: true
end
