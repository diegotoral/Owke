require 'rails_helper'

RSpec.describe Company::CreateOwner, type: :service do
  subject(:service) { described_class }

  context "on success" do
    it "returns company's owner" do
      user = build(:user)
      company = build(:company)

      allow(company).to receive(:save!)

      owner = service.call(company, user)

      expect(owner).to be_a User
      expect(owner).to be_persisted
    end

    it "sets user as company's owner" do
      user = build(:user)
      company = build(:company)

      allow(company).to receive(:save!)

      owner = service.call(company, user)

      expect(company.owner).to eq owner
    end
  end

  context "on error" do
    it "raises an exception" do
      user = build(:user, name: '')
      company = build(:company)

      expect{ service.call(company, user) }.to raise_exception ActiveRecord::RecordInvalid
    end
  end
end
