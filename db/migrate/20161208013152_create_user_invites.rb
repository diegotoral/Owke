class CreateUserInvites < ActiveRecord::Migration[5.0]
  def change
    create_table :user_invites do |t|
      t.string :name
      t.string :email
      t.string :invite_code, limit: 40
      t.datetime :invited_at
      t.datetime :redeemed_at

      t.timestamps

      t.index [:id, :email]
      t.index [:id, :invite_code]
    end
  end
end
