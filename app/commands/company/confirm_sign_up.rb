class Company::ConfirmSignUp < ::ApplicationCommand
  def call(subdomain, token)
    company = find_company_using_subdomain(subdomain)
    find_user_using_perishable_token(token).tap do |user|
      return broadcast(:error, 'User not found') if user.nil?

      user.confirmed!
      user.save!

      broadcast(:success, company)
    end
  end

  private

  def find_company_using_subdomain(subdomain)
    Company.find_by!(subdomain: subdomain).tap do |company|
      broadcast(:company_loaded, company)
    end
  rescue ActiveRecord::RecordNotFound => e
    broadcast(:company_not_found, e.message)
  end

  def find_user_using_perishable_token(token)
    User.find_using_perishable_token(token)
  end
end
