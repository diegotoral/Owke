FactoryGirl.define do
  factory :project do
    name 'Projeto 1'

    customer
    association :responsible, factory: :user
  end
end
