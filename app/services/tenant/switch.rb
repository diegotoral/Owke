class Tenant::Switch
  def self.call(company)
    Apartment::Tenant.switch! company.subdomain
  end
end
