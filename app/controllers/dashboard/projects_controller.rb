class Dashboard::ProjectsController < Dashboard::BaseController
  before_action :set_project_form, except: %i(index)

  def index
    @projects = Project::Finder.all
  end

  def new; end

  def create
    @project_form = ProjectForm.new(Project.new)

    if @project_form.validate(params[:project])
      @project_form.save
    end

    respond_with @project_form.sync, location: dashboard_projects_path
  end

  def edit; end

  def update
    if @project_form.validate(params[:project])
      @project_form.save
    end

    respond_with @project_form.sync, location: dashboard_projects_path
  end

  private

  def project
    @project ||= Project.find_or_initialize_by(id: params[:id])
  end

  def set_project_form
    @project_form ||= ProjectForm.new(project)
  end
end
