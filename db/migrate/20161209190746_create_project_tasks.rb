class CreateProjectTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :project_tasks do |t|
      t.string :title, null: false
      t.date :deadline
      t.text :description
      t.belongs_to :project, foreign_key: true, index: true
      t.belongs_to :responsible, references: :users, index: true

      t.timestamps
    end

    add_foreign_key :project_tasks, :users, column: :responsible_id
  end
end
