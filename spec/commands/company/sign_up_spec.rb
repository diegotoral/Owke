# require 'rails_helper'
#
# RSpec.describe Company::SignUp, type: :command do
#   subject(:command) { described_class.new }
#
#   context 'when everything goes fine' do
#     it 'creates a new company' do
#       expect(Company::Create).to receive(:call).with(company)
#
#       command.call(params)
#     end
#
#     it 'broadcasts :success' do
#       expect { command.call(params) }.to broadcast(:success, company)
#     end
#   end
#
#   context 'when something breaks' do
#     it 'broadcasts :error'
#   end
# end
