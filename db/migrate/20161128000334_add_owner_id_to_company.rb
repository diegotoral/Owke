class AddOwnerIdToCompany < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :owner_id, :integer
  end
end
