class User < ApplicationRecord
  has_many :team_memberships
  has_many :teams, through: :team_memberships, source: :team
  has_many :projects, foreign_key: :responsible_id

  validates :name, :initials, presence: true
  validates :email, presence: true, uniqueness: true

  acts_as_authentic do |c|
    c.crypto_provider = Authlogic::CryptoProviders::BCrypt
  end

  def confirmed!
    self[:confirmed] = true
  end
end
