require 'rails_helper'

RSpec.describe Dashboard::ProjectsController, type: :controller do
  describe 'GET on #index' do
    it 'responds with 200 OK' do
      login(create(:user, :active))

      get :index

      expect(response).to have_http_status 200
    end
  end

  describe 'GET on #new' do
    it 'responds with 200 OK' do
      login(create(:user, :active))

      get :new

      expect(response).to have_http_status 200
    end
  end

  describe 'POST on #create' do
    context 'when success' do
      it 'redirects to projects index' do
        user = create(:user, :active)
        login(user)
        project_params = build(:project).attributes.slice('name', 'customer_id')
        project_params[:global_responsible] = user.to_global_id

        post :create, params: { project: project_params }

        expect(response).to redirect_to dashboard_projects_path
      end
    end
  end

  describe 'GET on #edit' do
    it 'responds with 200 OK' do
      project = create(:project)
      login(create(:user, :active))

      get :edit, params: { id: project }

      expect(response).to have_http_status 200
    end
  end
end
