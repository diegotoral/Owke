class SignUpForm < Reform::Form
  property :name
  property :subdomain

  validates :subdomain, :name, presence: true

  property :owner do
    property :name
    property :email
    property :password
    property :password_confirmation

    validates :name, :email, presence: true
    validates :password, presence: true, confirmation: true
  end
end
