class User::Invite < ApplicationRecord
  validates :name, presence: true
  validates :email, presence: true, uniqueness: true

  scope :pending, -> { where(redeemed_at: nil) }

  def self.find_redeemable(invite_code)
    where(redeemed_at: nil, invite_code: invite_code).first
  end

  def pending?
    self[:redeemed_at].nil?
  end

  def invited?
    !!self[:invite_code] && !!self[:invited_at]
  end

  def invite!
    now = Time.current.utc
    self[:invite_code] = Digest::SHA1.hexdigest("--#{now}--#{self[:email]}--")
    self[:invited_at] = now
    save!
  end

  def redeemed!
    self[:redeemed_at] = Time.current.utc
    save!
  end

  def redeemed?
    self[:redeemed_at].present?
  end
end
