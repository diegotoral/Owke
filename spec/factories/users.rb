require 'ffaker'

FactoryGirl.define do
  factory :user do
    name { FFaker::Name.name }
    initials 'DT'
    password 'password'
    password_confirmation { password }
    password_salt { Authlogic::Random.hex_token }
    crypted_password { Authlogic::CryptoProviders::BCrypt.encrypt('password' + password_salt) }
    persistence_token { Authlogic::Random.hex_token }
    single_access_token { Authlogic::Random.friendly_token }
    perishable_token { Authlogic::Random.friendly_token }

    sequence(:email) {|n| "user#{n}@company.com" }

    trait :active do
      active true
      approved true
      confirmed true
    end
  end
end
