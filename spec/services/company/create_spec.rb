require 'rails_helper'

RSpec.describe Company::Create, type: :service do
  subject(:service) { described_class }

  context "on success" do
    it "returns the newly created company" do
      company = build(:company)

      company = service.call(company)

      expect(company).to be_a Company
      expect(company).to be_persisted
    end

    it "creates a tenant for the company's subdomain" do
      company = build(:company)
      tenant = double(:tenant)

      expect(tenant).to receive(:create).with(company[:subdomain])

      service.call(company, tenant: tenant)
    end
  end

  context "on error" do
    it "raises an exception" do
      company = Company.new

      expect{ service.call(company) }.to raise_exception ActiveRecord::RecordInvalid
    end
  end
end
