class ProjectForm < Reform::Form
  property :name
  property :customer_id
  property :global_responsible

  validates :name, :customer_id, :global_responsible, presence: true
end
