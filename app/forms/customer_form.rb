class CustomerForm < Reform::Form
  property :name
  property :email

  validates :name, presence: true
end
