# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170109134638) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "subdomain"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "owner_id"
    t.index ["name", "subdomain"], name: "index_companies_on_name_and_subdomain", using: :btree
  end

  create_table "customers", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "project_tasks", force: :cascade do |t|
    t.string   "title",                      null: false
    t.date     "deadline"
    t.text     "description"
    t.integer  "project_id"
    t.integer  "responsible_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "status",         default: 0
    t.index ["project_id"], name: "index_project_tasks_on_project_id", using: :btree
    t.index ["responsible_id"], name: "index_project_tasks_on_responsible_id", using: :btree
    t.index ["status"], name: "index_project_tasks_on_status", using: :btree
  end

  create_table "projects", force: :cascade do |t|
    t.string   "name",             null: false
    t.date     "deadline"
    t.text     "description"
    t.integer  "customer_id"
    t.integer  "responsible_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "responsible_type"
    t.index ["customer_id"], name: "index_projects_on_customer_id", using: :btree
    t.index ["responsible_id"], name: "index_projects_on_responsible_id", using: :btree
  end

  create_table "team_memberships", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "team_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["team_id"], name: "index_team_memberships_on_team_id", using: :btree
    t.index ["user_id", "team_id"], name: "index_team_memberships_on_user_id_and_team_id", using: :btree
    t.index ["user_id"], name: "index_team_memberships_on_user_id", using: :btree
  end

  create_table "teams", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_invites", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "invite_code", limit: 40
    t.datetime "invited_at"
    t.datetime "redeemed_at"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["id", "email"], name: "index_user_invites_on_id_and_email", using: :btree
    t.index ["id", "invite_code"], name: "index_user_invites_on_id_and_invite_code", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.string   "persistence_token"
    t.string   "single_access_token"
    t.string   "perishable_token"
    t.integer  "login_count",         default: 0,     null: false
    t.integer  "failed_login_count",  default: 0,     null: false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip"
    t.string   "last_login_ip"
    t.boolean  "active",              default: false
    t.boolean  "approved",            default: false
    t.boolean  "confirmed",           default: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "initials",            default: ""
    t.index ["email"], name: "index_users_on_email", using: :btree
  end

  add_foreign_key "project_tasks", "projects"
  add_foreign_key "project_tasks", "users", column: "responsible_id"
  add_foreign_key "projects", "customers"
  add_foreign_key "projects", "users", column: "responsible_id", on_delete: :nullify
  add_foreign_key "team_memberships", "teams"
  add_foreign_key "team_memberships", "users"
end
