//= require jquery
//= require jquery_ujs
//= require iziModal

//= require dashboard/flashes

window.Owke = window.Owke || {};

$(function() {
  Owke.Flashes.init();

  $('#new-task-modal').iziModal({
    title: 'Nova tarefa',
    headerColor: '#0cc2aa',
    autoOpen: false,
  });

  $('#task-modal').iziModal({
    title: 'Exibindo tarefa',
    headerColor: '#0cc2aa',
    autoOpen: false,
  });

  $(".dropdown-action").click(function() {
    var $this = $(this)

    $(".dropdown-content").removeClass("dropdown-open");
    $this.next().toggleClass("dropdown-open");
  });

  $('body').on('click', function(e) {
    if($(e.target).closest('.dropdown-action, .dropdown-content').length == 0) {
      console.log('close')
      $(".dropdown-content").removeClass("dropdown-open");
    }
  });
});
