class AddStatusToProjectTasks < ActiveRecord::Migration[5.0]
  def change
    add_column :project_tasks, :status, :integer, default: 0
    add_index :project_tasks, :status
  end
end
