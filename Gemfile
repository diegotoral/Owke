source 'https://rubygems.org'

ruby '2.3.1'

# Core gems
gem 'apartment'
gem 'authlogic'
gem 'bcrypt'
gem 'enum_help'
gem 'pg', '~> 0.18'
gem 'puma', '~> 3.0'
gem 'rails', '~> 5.0.0', '>= 5.0.0.1'
gem 'reform'
gem 'reform-rails'
gem 'wisper', '2.0.0.rc1'
gem 'responders'

# Assets gems
gem 'animate-rails'
gem 'bourbon', '~> 4.2'
gem 'browserify-rails'
gem 'izimodal'
gem 'jquery-rails'
gem 'material_icons'
gem 'font-awesome-rails'
gem 'neat', '~> 1.7'
gem 'normalize-rails'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'capybara'
  gem 'database_cleaner'
  gem 'factory_girl_rails', '~> 4.0'
  gem 'ffaker'
  gem 'poltergeist'
  gem 'rspec-rails', '~> 3.5'
  gem 'shoulda-matchers', '~> 3.1'
  gem 'site_prism'
  gem 'wisper-rspec', require: false
end

group :development do
  gem 'bullet'
  gem 'listen', '~> 3.0.5'
  gem 'simplecov', require: false
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
