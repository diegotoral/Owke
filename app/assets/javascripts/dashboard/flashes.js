(function($, Owke) {
  'use strict';

  var defaultOptions = {
    width: 600,
    timeout: 10000,
    autoOpen: true,
    overlay: false,
    attached: 'top',
    group: 'flashes',
    pauseOnHover: true,
    pauseOnHover: true,
    icon: 'material-icons',
    timeoutProgressbar: true,
    transitionIn: 'fadeInDown',
    transitionOut: 'fadeOutDown',
    navigateArrows: 'closeScreenEdge',
  };

  var Flashes = {
    init: function() {
      console.debug('Initializing Owke.Flashes...');

      $('.flash.flash--alert').iziModal($.extend({}, defaultOptions, {
        headerColor: '#BD5B5B',
        iconText: 'info outline',
      }));

      $('.flash.flash--notice').iziModal($.extend({}, defaultOptions, {
        headerColor: '#00af66',
        iconText: 'check',
      }));
    }
  };

  Owke.Flashes = Flashes;

})(jQuery, window.Owke = window.Owke || {});
