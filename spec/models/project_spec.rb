require 'rails_helper'

RSpec.describe Project, type: :model do
  it { is_expected.to belong_to :customer }
  it { is_expected.to belong_to :responsible }
end
