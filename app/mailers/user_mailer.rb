class UserMailer < ApplicationMailer
  def invite(company_id, invite_id)
    @invite = User::Invite.find(invite_id)
    @company = Company.find(company_id)

    mail(to: @invite.email, subject: 'Owke invite')
  end
end
