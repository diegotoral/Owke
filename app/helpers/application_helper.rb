module ApplicationHelper
  def active_class(name)
    if controller_name === name
      "active"
    end
  end

  def nav_new_item(action)
    content_tag 'li' do
      link_to action, class: 'nav-new-item' do
        "#{material_icon.add_circle}".html_safe
      end
    end
  end
end
