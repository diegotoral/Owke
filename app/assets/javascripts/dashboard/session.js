//= require jquery
//= require jquery_ujs
//= require iziModal

//= require dashboard/flashes

window.Owke = window.Owke || {};

$(function() {
  Owke.Flashes.init();

  $('form input').blur(function(){
    if($(this).val()) {
      $(this).addClass('input-focus');
    } else {
      $(this).removeClass('input-focus');
    }
  });
});
